<?php

namespace Techendeavors\CheckPCIID;

use Illuminate\Support\Collection;
use Techendeavors\DNSOverHttps\DNSOverHttps;

class CheckPCIID
{
    private const HOST_ROOT  = ".pci.id.ucw.cz";

    public static function search($query = null)
    {
        if (strlen($query) == 4) {
            $vendorID = $query;
        }

        if (strlen($query) == 8) {
            $exploded = str_split($query, 4);
            $vendorID = $exploded[0];
            $deviceID = $exploded[1];
        }

        if (strlen($query) == 9 && str_contains($query, ':')) {
            $exploded = explode(":", $query);
            $vendorID = $exploded[0];
            $deviceID = $exploded[1];
        }

        $check = new DNSOverHttps;
        $check->records(['TXT']);
        if (!empty($vendorID)) {
            $check->domain($vendorID.self::HOST_ROOT);
        }
        if (!empty($deviceID)) {
            $check->domain($deviceID.'.'.$vendorID.self::HOST_ROOT);
        }
        $results = $check->check();

        foreach($results as $item) {
            $length = strlen(substr($item['domain'], 0, -strlen(self::HOST_ROOT)));
            if ($length == 4) {
                $output['Vendor'] = substr($item['records']['TXT'][0], 3, -1);
            }
            if ($length == 9) {
                $output['Device'] = substr($item['records']['TXT'][0], 3, -1);
            }
        }

        return $output;
    }
}
