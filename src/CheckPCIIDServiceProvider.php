<?php

namespace Techendeavors\CheckPCIID;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Container\Container;

class CheckPCIIDServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->alias('checkpciid', CheckPCIID::class);

        $this->app->singleton('checkpciid', function (Container $app) {
            return new CheckPCIID();
        });        
    }
}
