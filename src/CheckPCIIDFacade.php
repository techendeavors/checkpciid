<?php

namespace Techendeavors\CheckPCIID\Facades;

use Illuminate\Support\Facades\Facade;

class CheckPCIIDFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'checkpciid';
    }
}
