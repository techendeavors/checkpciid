# CheckPCIID

## Returns the vendor and device name when supplied with a PCI device ID. 

[![Latest Version on Packagist](https://img.shields.io/packagist/v/techendeavors/checkpciid.svg?style=flat-square)](https://packagist.org/packages/techendeavors/checkpciid)
[![Total Downloads](https://img.shields.io/packagist/dt/techendeavors/checkpciid.svg?style=flat-square)](https://packagist.org/packages/techendeavors/checkpciid)

Doesn't have a local database, this pulls data from DNS TXT records using the `techendeavors/dnsoverhttps` package. 

### To Do

- [ ] Make it able to be called statically
- [ ] Write Tests

### Installation

You can install the package via composer:

```bash
composer require techendeavors/checkpciid
```

### Usage

```
>>> $id = new CheckPCIID;
=> Techendeavors\CheckPCIID\CheckPCIID {#2863}
>>> $results = $id->search('10ec:8168')
=> [
     "Vendor" => "Realtek Semiconductor Co., Ltd.",
     "Device" => "RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller",
   ]
>>> $results = $id->search('10ec')
=> [
     "Vendor" => "Realtek Semiconductor Co., Ltd.",
   ]
>>> $results = $id->search('10de:1b80')
=> [
     "Vendor" => "NVIDIA Corporation",
     "Device" => "GP104 [GeForce GTX 1080]",
   ]
>>> $results = $id->search('8086:591f')
=> [
     "Device" => "Xeon E3-1200 v6/7th Gen Core Processor Host Bridge/DRAM Registers",
     "Vendor" => "Intel Corporation",
   ]
```

### Testing
Not yet implemented

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

### Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please use the [issue tracker](https://gitlab.com/techendeavors/checkpciid)

### Credits

- [Techendeavors](https://gitlab.com/techendeavors)
- [Contributors and Credits](CONTRIBUTORS.md)



### License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
